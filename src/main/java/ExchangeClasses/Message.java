package ExchangeClasses;

import javax.persistence.*;

@Entity
@Table(name = "Message", uniqueConstraints={@UniqueConstraint(columnNames={"_id"})})
public class Message {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="_id", nullable=false, unique = true, length = 11)
    private Long _id;

    @Column(name = "text")
    private String text = "";

    @Column(name = "sender")
    private String sender = "";

    @Column(name = "previousID")
    private Long previous;

    public Message(String test, String from){
        this.text = test;
        this.sender =from;
    }
    public Message(){}
    public Message(String text) {
        this.text = text;
    }

    public void setSender(String from) {
        this.sender = from;
    }

    public String getSender() {
        return sender;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }


    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public Long getPrevious() {
        return previous;
    }

    public void setPrevious(Long previous) {
        this.previous = previous;
    }
}
